local M = {}

M.general = {
  i = {
    -- go to beginning
    ["<C-a>"] = { "<ESC>^i", "Beginning of line" },

    ["jj"] = {"<ESC>", "switch to normal mode"}
  },
}

M.dap = {
  plugin = true,
  n = {
    ["<leader>db"] = {
      "<cmd> DapToggleBreakpoint <CR>",
      "Add breakpoint at line",
    },
    ["<leader>dr"] = {
      "<cmd> DapContinue <CR>",
      "Start or continue the debugger",
    }
  }
}

M.telescope = {
  plugin = true,

  n = {
    -- find
    ["<leader>ff"] = { "<cmd> Telescope find_files <CR>", "Find files" },
    ["<leader>fa"] = { "<cmd> Telescope find_files follow=true no_ignore=true hidden=true <CR>", "Find all files" },
    ["<leader>fw"] = { "<cmd> Telescope live_grep <CR>", "Live grep" },
    ["<leader>fb"] = { "<cmd> Telescope buffers <CR>", "Find buffers" },
    ["<leader>fh"] = { "<cmd> Telescope help_tags <CR>", "Help page" },
    ["<leader>fr"] = { "<cmd> Telescope oldfiles <CR>", "Find oldfiles" },
    ["<leader>fo"] = { "<cmd> Telescope oldfiles <CR>", "Find oldfiles" },
    ["<leader>fz"] = { "<cmd> Telescope current_buffer_fuzzy_find <CR>", "Find in current buffer" },
    ["<leader>ft"] = { "<cmd> Telescope terms <CR>", "Pick hidden term" },

    -- git
    ["<leader>gcm"] = { "<cmd> Telescope git_commits <CR>", "Git commits" },
    ["<leader>gbcm"] = { "<cmd> Telescope git_bcommits <CR>", "Git buffer commits" },
    ["<leader>gbcr"] = { "<cmd> Telescope git_bcommits_range <CR>", "Git buffer commits range" },
    ["<leader>gst"] = { "<cmd> Telescope git_status <CR>", "Git status" },
    ["<leader>gbr"] = { "<cmd> Telescope git_branches <CR>", "Git branches" },

    -- theme switcher
    ["<leader>th"] = { "<cmd> Telescope themes <CR>", "Nvchad themes" },

    -- bookmarks
    ["<leader>ma"] = { "<cmd> Telescope marks <CR>", "telescope bookmarks" },

    -- registers
    ["<leader>rg"] = { "<cmd> Telescope registers <CR>", "telescope registers" },

    -- Lists Function names, variables, from Treesitter!
    ["<leader>ts"] = { "<cmd> Telescope treesitter <CR>", "telescope outline from treesitter" },
    ["<leader>fs"] = { "<cmd> Telescope lsp_document_symbols <CR>", "telescope buffer symbols from lsp" },
    ["<leader>fas"] = { "<cmd> Telescope lsp_workspace_symbols <CR>", "telescope workspace symbols from lsp" },

  },
}
return M
