## 介绍

这个仓库用来备份和同步我的所有配置文件以及程序包列表，你可以查看这个视频[如何快速配置nvim开发环境](https://www.bilibili.com/video/BV1h6421g7dV/?share_source=copy_web&vd_source=9ae54c31182cbbfa8dbdc757855a2545)，视频中简单演示了如何配置nvim

本仓库包含nvim,tmux,git,bashrc,cmake等的配置文件，以及一些子模块，用于自动安装软件，还有一些包名列表，方便同步。

这些配置文件都是用过stow来软链接到对应的目录下，可以理解为每一个一级目录都是一个软件包的配置，然后在该软件包中的目录结构就相当于在$HOME目录下的软链接地址。通过命令`stow */`把所有的当前目录下的目录都正确软链接到对应的$HOME目录下。

## 安装

使用以下命令将本仓库下载到`~/dotfiles/`目录下

```shell
git clone --depth 1 --recursive https://gitee.com/ericling666/dotfiles ~/dotfiles
```
> 注意：这里`--depth 1` 表示仅下载最新的提交而不下载历史提交，`--recursive`表示下载仓库中引用的子模块
> 另外，dotfiles项目必须放在`～`目录下，否则需要额外的参数，因为stow默认就是软链接到上一层目录下的对应地址。

然后执行以下命令安装apt的程序包


```shell
sudo xargs apt install <.apt-list -y
```

然后执行以下命令，安装rust编译器和cargo

```shell
bash .rustup-install.sh
```

然后执行以下命令，使用cargo安装相应的程序包
```shell
xargs cargo install <.cargo-list
```

然后执行以下命令，安装fzf

```shell
bash .fzf-install.sh
```


然后执行以下命令，安装lazydocker

```shell
bash .lazydocker-install.sh
```


然后执行以下命令，安装lazygit

```shell
bash .lazygit-install.sh
```
然后执行以下命令，将所有应用程序的配置文件都软链接到对应目录下
```shell
stow */
```

打开tmux，使用快捷键'Ctrl-b I' 安装tmux的插件

打开nvim，会自动下载lazy.nvim，然后lazy.nvim会自动下载其它所有的插件，`:q`退出nvim之后，再重新打开nvim，输入`:MasonInstallAll`将会用Mason安装配置好的lsp server，dap server，formatter

> 注意：需要nvim使用0.9版本以上，ubuntu默认安装的版本有一些配置有bug，所以需要手动编译，请参考官方文档构建
> nvim配置需要搭配Nerd Font，这样配置好的一些图标才能正常显示，推荐使用[JetBrainsMono.zip](https://github.com/ryanoasis/nerd-fonts/releases/download/v3.1.1/JetBrainsMono.zip), 下载好字体后，安装到本机，然后windows Terminal或者其它终端模拟器软件中配置一下对应的字体



