source ~/.local/gdb-dashboard/.gdbinit


define soul
  dashboard -layout source stack variables
  dashboard source -style height 20
end

define asml
  dashboard -layout registers assembly memory stack
end

